//
//  ViewController.swift
//  Kolik
//
//  Created by Margulan Sugirbay on 12/21/20.
//  Copyright © 2020 Kolik App. All rights reserved.
//

import UIKit
import MapKit
import FloatingPanel

class MainMapViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    
    let fpc = FloatingPanelController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        congigureFpc()
    }

}

// MARK:- FloatingPanel Layout
extension MainMapViewController: FloatingPanelControllerDelegate {
    
    func congigureFpc() {
        guard let mapSearchVC = storyboard?.instantiateViewController(identifier: "MapSearchVC") as? MapSearchViewController else { return }
        fpc.delegate = self
        // todo fix topOffset
        fpc.layout = MapSearchInitialLayout(count: 2, heightLimit: view.frame.height, topOffset: 0)
        fpc.contentMode = .fitToBounds
//        fpc.track(scrollView: mapSearchVC.tableView)
        
        let appearance = SurfaceAppearance()
        appearance.cornerRadius = 20
        appearance.backgroundColor = .white
        fpc.surfaceView.appearance = appearance
        
        fpc.set(contentViewController: mapSearchVC)
        fpc.addPanel(toParent: self)
    }
}
