//
//  MapSearchViewController.swift
//  Kolik
//
//  Created by Margulan Sugirbay on 12/22/20.
//  Copyright © 2020 Kolik App. All rights reserved.
//

import UIKit

class MapSearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }

}

extension MapSearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TitleDividerTableViewCell
        cell.dividerTitle.text = "Near You"
        return cell
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TitleDividerTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
//        tableView.rowHeight = 64
        tableView.bounces = false
        
    }
    
    
}
