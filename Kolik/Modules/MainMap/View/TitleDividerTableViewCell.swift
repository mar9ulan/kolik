//
//  TitleDividerTableViewCell.swift
//  Kolik
//
//  Created by Sugirbay Margulan on 12/22/20.
//  Copyright © 2020 Kolik App. All rights reserved.
//

import UIKit

class TitleDividerTableViewCell: UITableViewCell {

    @IBOutlet weak var dividerTitle: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
