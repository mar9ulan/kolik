//
//  MapSearchInitialLayout.swift
//  Kolik
//
//  Created by Sugirbay Margulan on 12/22/20.
//  Copyright © 2020 Kolik App. All rights reserved.
//

import Foundation
import FloatingPanel

class MapSearchInitialLayout: FloatingPanelLayout {
    
    let position: FloatingPanelPosition = .bottom
    let initialState: FloatingPanelState = .tip
    
    private let list: [FloatingPanelState: FloatingPanelLayoutAnchoring]
    
    var anchors: [FloatingPanelState: FloatingPanelLayoutAnchoring] {
        return list
    }
    
    init(count: Int, heightLimit: CGFloat, topOffset: CGFloat) {
        switch count {
        case 1:
            list = [.tip: FloatingPanelLayoutAnchor(absoluteInset: 128.0, edge: .bottom, referenceGuide: .safeArea)]
        default:
            if heightLimit - topOffset <= 64.0 + CGFloat(count) * 64.0 {
                list = [
                    .half: FloatingPanelLayoutAnchor(absoluteInset: topOffset, edge: .top, referenceGuide: .safeArea),
                    .tip: FloatingPanelLayoutAnchor(absoluteInset: 156.0, edge: .bottom, referenceGuide: .superview)]
            } else {
                list = [
                    .half: FloatingPanelLayoutAnchor(absoluteInset: 64.0 + CGFloat(count) * 64.0, edge: .bottom, referenceGuide: .safeArea),
                    .tip: FloatingPanelLayoutAnchor(absoluteInset: 156.0, edge: .bottom, referenceGuide: .superview)]
            }
        }
    }
}
